import logo from './logo.svg';
import logo2 from './random-profile.svg';
import './App.css';
import React, { useState } from 'react';

function App() {
  // Helper Functions
  const setQueryStringWithNoReload = qsValue => {
    const newURL = window.location.protocol + '//' +
      window.location.host +
      window.location.pathname +
      qsValue;
    window.history.pushState({ path: newURL }, "", newURL);
  }
  const urlSearch = window.location.search
  let urlParams = new URLSearchParams(urlSearch);
  const urlWords = urlParams.get('words');
  const urlCount = urlParams.get('count');

  const [wordlist, setWordlist] = useState(
      urlWords ? urlWords.split(',') :
      ['Name', 'Gen', 'Random', 'Generator', 'Online', 'Shuffle', 'Web']
  );
  const [curName, setCurName] = useState('Namegen');
  const [curCount, setCurCount] = useState(urlCount ? urlCount : 2);
  const [pastNames, setPastNames] = useState([]);
  const handleChange = e => {
    const newRes = e.target.value.split('\n');
    setWordlist(newRes);
  }

  const handleSliderChange = e => {
    const newCount = e.target.value;
    setCurCount(newCount);
  }
  const generate = () => {
    setPastNames(pastNames.concat(curName));
    let newName = '';
    for (let i = 0; i < curCount; i++) {
      newName += '' + wordlist[Math.floor(Math.random()*wordlist.length)];
    }
    setCurName(newName);
    return;
  }
  const save = () => {
    setQueryStringWithNoReload(`?count=${curCount}&words=${wordlist}`)
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }
  const inputStyles = {
    margin: '1em',
    width: '90%',
  };
  return (
    <div className="App">
      <header className="App-header">
        <h1>{curName}</h1>
        <input type='button' value='Click to Generate' onClick={generate} className='generateButton' />
        <div style={{position:'relative', top: 0, left: 0}} onClick={generate} className='generateButton'>
          <img src={logo} className="App-logo" alt="logo" style={{position:'relative', top: 0, left: 0}} />
          <img src={logo2} className="App-logo2" alt="logo2" style={{position:'absolute', top: '0%', left: '0%'}} />
        </div>
        {/*<div onClick={generate} className='generateButton'>
          <img src={logo} className="App-logo" alt="logo" />
          <p>~ tap to generate ~</p>
        </div>*/}
        {pastNames && pastNames.length > 0 &&
          <>
          <p style={{fontSize: '0.5em', borderBottom: '1px solid grey', marginBottom: '-5px'}}>Past Names:</p>
          <div className='pastNames'>
            {pastNames.map(x => (
              <div>
                {x}
              </div>
            )).reverse()}
          </div>
          </>
        }
      </header>
      <p>Words: {wordlist.length}</p>
      <textarea style={inputStyles} label='words' name='words' textarea rows={10} onChange={handleChange} value={wordlist.join('\n')}/>
      <p>Count: {curCount}</p>
      <input type='range' min='1' max='10' style={inputStyles} label='count' name='count' onChange={handleSliderChange} value={curCount} /> 
      <input type='button' value='Save settings into URL' onClick={save} className='generateButton' />
      <p>
        Thanks for stopping by!
      </p>
    </div>
  );
}

export default App;
